# Tets task

## Установка

```shell
python3.6 -m venv ./venv
. ./venv/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
```

## Запуск

```
./manage.py migrate
./manage.py loaddata db.json
./manage.py runserver
```

## Открываем

```
http://127.0.0.1:8000/admin/

login:q
pwd:1
```
