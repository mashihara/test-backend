from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include

from rest_framework import routers

from shops.api import views as shops_api


router = routers.DefaultRouter()
router.register(r'products', shops_api.ProductViewSet)
router.register(r'shops', shops_api.ShopViewSet)
router.register(r'documents', shops_api.DocumentViewSet)
router.register(r'document_types', shops_api.DocumentTypeViewSet)

from django.views.decorators.csrf import csrf_exempt


urlpatterns = [
    path('admin/', admin.site.urls),

    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]
