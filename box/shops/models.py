import datetime

from django.db import models
from django.contrib.auth.models import User


class DateTimeMixin:

    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)


class Shop(models.Model, DateTimeMixin):

    name = models.CharField('Наименование', max_length=255)

    class Meta:
        ordering = ('-id',)

    def __str__(self):
        return self.name


class Product(models.Model, DateTimeMixin):

    name = models.CharField('Наименование', max_length=255)

    class Meta:
        ordering = ('-id',)

    def __str__(self):
        return self.name


class DocumentType(models.Model, DateTimeMixin):

    name = models.CharField('Наименование', max_length=255)

    def __str__(self):
        return self.name


class Document(models.Model, DateTimeMixin):

    shop = models.ForeignKey(Shop, verbose_name='Магазин', on_delete=models.CASCADE)
    type = models.ForeignKey(DocumentType, verbose_name='Тип документа', on_delete=models.CASCADE)
    products = models.ManyToManyField(Product, verbose_name='Товары', through='ProductQuantity')

    date = models.DateField(verbose_name='Дата', default=datetime.date.today)
    accepted_user = models.ForeignKey(User, verbose_name='Принимающий', on_delete=models.CASCADE)

    class Meta:
        ordering = ('-id',)

    def __str__(self):
        return f'{self.type} - {self.shop} | user: {self.accepted_user} | {self.date}'


class ProductQuantity(models.Model):
    document = models.ForeignKey(Document, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)

    quantity = models.PositiveIntegerField('Количество')

