from django.contrib import admin

from .models import Document, DocumentType, Shop, Product


for clazz in DocumentType, Document, Shop, Product:
    admin.site.register(clazz)
