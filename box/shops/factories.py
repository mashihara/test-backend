import factory
import factory.django

from . import models


class ShopFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Shop

    name = factory.Faker('company')


class ProductFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Product

    name = factory.Faker('slug')


class DocumentTypeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.DocumentType

    name = factory.Iterator(['one', 'two', 'three', 'four', 'five'])

