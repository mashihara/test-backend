from rest_framework import viewsets
from django_filters.rest_framework import DjangoFilterBackend

from .serializers import DocumentSerializer, ProductSerializer, ShopSerializer, DocumentTypeSerializer
from .filters import DocumentRawSQLFilter
from ..models import Document, Product, Shop, DocumentType

from rest_framework import filters


class ProductViewSet(viewsets.ModelViewSet):
    serializer_class = ProductSerializer
    queryset = Product.objects.all()

    filter_backends = (filters.SearchFilter,)
    search_fields = ('^name',)


class ShopViewSet(viewsets.ModelViewSet):
    serializer_class = ShopSerializer
    queryset = Shop.objects.all()

    filter_backends = (filters.SearchFilter,)
    search_fields = ('name',)


class DocumentTypeViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = DocumentTypeSerializer
    queryset = DocumentType.objects.all()


class DocumentViewSet(viewsets.ModelViewSet):
    serializer_class = DocumentSerializer
    queryset = Document.objects.all()

    filter_backends = (DjangoFilterBackend,)
    filterset_class = DocumentRawSQLFilter

    def perform_create(self, serializer):
        serializer.save(accepted_user=self.request.user)

    def perform_update(self, serializer):
        serializer.save(accepted_user=self.request.user)

