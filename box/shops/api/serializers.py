from django.contrib.auth.models import User
from rest_framework import serializers

from ..models import Document, Shop, Product, DocumentType, ProductQuantity


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('pk', 'username')


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'


class ShopSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shop
        fields = '__all__'


class DocumentTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = DocumentType
        fields = '__all__'


class ProductQuantitySerializer(serializers.ModelSerializer):

    name = serializers.CharField(source='product.name')

    class Meta:
        model = ProductQuantity
        fields = '__all__'


class DocumentSerializer(serializers.ModelSerializer):

    type_name = serializers.SlugField(source='type.name', read_only=True)
    shop_name = serializers.SlugField(source='shop.name', read_only=True)

    accepted_user = UserSerializer(read_only=True)
    products = ProductQuantitySerializer(source='productquantity_set', many=True, read_only=True)

    class Meta:
        model = Document
        fields = ('id', 'shop', 'type', 'products', 'date', 'accepted_user',
                  'type_name', 'shop_name')
