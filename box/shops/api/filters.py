from django_filters import rest_framework as filters

from ..models import Document


class DocumentRawSQLFilter(filters.FilterSet):
    date_start = filters.DateFilter(field_name="date", lookup_expr='gte')
    date_end   = filters.DateFilter(field_name="date", lookup_expr='lte')

    class Meta:
        model = Document
        fields = ('shop', 'accepted_user', 'date', 'type', 'date_start', 'date_end')
